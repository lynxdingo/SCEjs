const selectFellow = document.querySelector("#ribbonFellow");
const inputFellow = document.querySelector("#ribbonFellowAmount");
const selectFox = document.querySelector("#ribbonFox");
const inputFox = document.querySelector("#ribbonFoxAmount");

function RibbonBuilder(obj) {
    this.obj = obj;
}

var ribbonCorp = {
    fellowType: "Bierband",
    fellowAmount: "-1",
    fellowGrounds: [],
    fellowColors: [],
    foxType: "Bierband",
    foxAmount: "-1",
    foxGrounds: [],
    foxColors: [],
}

var objFellow = {
    id: "ribbonFellowDefiner",
    name: "Fellow",
    amount: "0"
}

var objFox = {
    id: "ribbonFoxDefiner",
    name: "Fox",
    amount: "0"
}

var initialColors = {
    "black": "#000000",
    "white": "#ffffff",
    "silver": "#c0c0c0",
    "gold": "#ffd700",
    "red": "#ff0000",
    "green": "#008000",
    "blue": "#0000ff",
};

RibbonBuilder.createDefiner = function (obj) {
    const count = parseInt(obj.amount) + 2;
    $("<div/>", {
        "id": "colorRow" + obj.name
    }).appendTo("#" + obj.id);
    for (let i = 0; i < count; i++) {
        $("<div/>", {
            "class": "form-group row",
            "id": "colorRow" + obj.name + i
        }).appendTo("#colorRow" + obj.name);
    }
    RibbonBuilder.createColorRow(obj);
}

RibbonBuilder.createColorRow = function (obj) {
    const count = parseInt(obj.amount);
    RibbonBuilder.colorGrundRow({ name: obj.name, number: "0" });
    for (let i = 1; i < count + 1; i++) {
        $("<label/>", {
            "class": "col-sm-2 col-form-label",
            "for": "color" + obj.name + i,
            "html": "Farbe " + i
        }).appendTo("#colorRow" + obj.name + i);
        $("<input/>", {
            "class": "form-control col-sm-3",
            "id": "color" + obj.name + i,
            "type": "text"
        }).appendTo("#colorRow" + obj.name + i);

        //Adds a colorpicker to each color input box
        $("#color" + obj.name + i).colorpicker({
        defaultPalette: 'web',
        initialHistory: [initialColors.black, initialColors.white, initialColors.silver, initialColors.gold, initialColors.red, initialColors.green, initialColors.blue],
        showOn: 'focus'
    });
    }
    RibbonBuilder.colorGrundRow({ name: obj.name, number: (count + 1) });
}

RibbonBuilder.colorGrundRow = function (obj) {
    var number = "";
    if (obj.number == 0) {
        number = "oben";
    } else {
        number = "unten";
    }
    $("<label/>", {
        "class": "col-sm-2 col-form-label",
        "for": "colorGrund" + obj.name + obj.number,
        "html": "Grund " + number
    }).appendTo("#colorRow" + obj.name + obj.number);
    $("<input/>", {
        "class": "form-control col-sm-3",
        "id": "colorGrund" + obj.name + obj.number,
        "type": "text"
    }).appendTo("#colorRow" + obj.name + obj.number);

    //Adds a colorpicker color input box
    $("#colorGrund" + obj.name + obj.number).colorpicker({
        defaultPalette: 'web',
        initialHistory: [initialColors.black, initialColors.white, initialColors.silver, initialColors.gold, initialColors.red, initialColors.green, initialColors.blue],
        showOn: 'focus'
    });
}

RibbonBuilder.createFellow = function () {
    if (ribbonCorp.fellowAmount == "-1") {//checks if input already exists
        ribbonCorp.fellowAmount = "" + Math.abs(parseInt(inputFellow.value));
        if (ribbonCorp.fellowAmount >= 6) {
            window.alert("That value is too great! Please choose a value less than 6.");
            return;
        }
        objFellow.amount = ribbonCorp.fellowAmount;
        RibbonBuilder.createDefiner(objFellow);
    } else {
        var elem = document.getElementById("colorRowFellow");
        elem.parentNode.removeChild(elem);
        ribbonCorp.fellowAmount = "-1";
        RibbonBuilder.createFellow();
    }
}

RibbonBuilder.createFox = function () {
    if (ribbonCorp.foxAmount == "-1") {//checks if input already exists
        ribbonCorp.foxAmount = "" + Math.abs(parseInt(inputFox.value));
        if (ribbonCorp.foxAmount >= 6) {
            window.alert("That value is too great! Please choose a value less than 6.");
            return;
        }
        objFox.amount = ribbonCorp.foxAmount;
        RibbonBuilder.createDefiner(objFox);
    } else {
        var elem = document.getElementById("colorRowFox");
        elem.parentNode.removeChild(elem);
        ribbonCorp.foxAmount = "-1";
        RibbonBuilder.createFox();
    }

}
