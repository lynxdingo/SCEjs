function CardBuilder(obj, col) {
  this.obj = obj;
  this.col = col;
}

var stdObj = {
  id: null,
  text: null,
  appendId: null,
  tag: null,
  h: null,
};

CardBuilder.createCardEvent = function (event) {
  stdObj.id = event.eventID;
  const cardId = CardBuilder.createDivCard(stdObj);

  stdObj.appendId = cardId;
  stdObj.text = event.title;
  CardBuilder.createDivHeader(stdObj);
  CardBuilder.createNavHeader(stdObj);

  stdObj.appendId = CardBuilder.createDivBody(stdObj);
  stdObj.appendId = CardBuilder.createDivTabContent(stdObj);
  CardBuilder.createCardTabPanels(stdObj);

  CardBuilder.createOverviewTab(stdObj, event);
  CardBuilder.createDetailsTab(stdObj, event);
  CardBuilder.createAboutTab(stdObj, event);
}

CardBuilder.createOverviewTab = function (obj, event) {
  obj.appendId = "cardTabPanel-Overview-" + event.eventID;
  var tempStr = "";
  obj.id = tempStr.concat("Overview-", event.eventID);
  if (event.timeEnd != "" && event.timeEnd != null) {
    obj.text = event.date + ", " + event.timeStart + " - " + event.timeEnd;
  } else {
    obj.text = event.date + ", " + event.timeStart;
  }
  CardBuilder.createTitle(obj);

  obj.text = event.description;
  CardBuilder.createText(obj);
}

CardBuilder.createDetailsTab = function (obj, event) {
  obj.appendId = "cardTabPanel-Details-" + event.eventID;
  var tempStr = "";
  obj.id = tempStr.concat("Details-", event.eventID);

  obj.text = " Details to Event:";
  CardBuilder.createText(obj);
}

CardBuilder.createAboutTab = function (obj, event) {
  obj.appendId = "cardTabPanel-About-" + event.eventID;
  var tempStr = "";
  obj.id = tempStr.concat("About-", event.eventID);

  obj.text = " About Corporation:";
  CardBuilder.createText(obj);
}

getEventData = function () {
  var i = 0, keys = [], key = "", row = {}, el = {};
  //load all book objects
  EventCorp.loadAll();
  keys = Object.keys(EventCorp.instances);
  //for each book, create a table row with a cell for each attribute
  for (i = 0; i < keys.length; i++) {
    el = EventCorp.instances[keys[i]];
    CardBuilder.createCardEvent(el);
  }
}

CardBuilder.createDivCard = function (obj) {
  $("<div/>", {
    "class": "card text-white bg-primary mb-3",
    "style":"margin-left:auto; margin-right:auto;",
    "id": "card" + obj.id
  }).appendTo("#eventCards");//should be changed for generaliztion
  return "card" + obj.id;
}

CardBuilder.createDivHeader = function (obj) {
  $("<div/>", {
    "class": "card-header",
    "id": "cardHeader" + obj.id,
    "html": obj.text,
  }).appendTo("#" + obj.appendId);
  return "cardHeader" + obj.id;
}

CardBuilder.createNavHeader = function (obj) {
  const navTitles = ["Overview", "Details", "About"];
  //creates <ul>
  $("<ul/>", {
    "class": "nav nav-tabs card-header-tabs",
    "style":"margin-left:-1px; margin-right:auto;",
    "role": "tablist",
    "id": "cardNav" + obj.id
  }).appendTo("#" + obj.appendId);
  //creates <li>
  for (var i = 0; i < navTitles.length; i++) {
    $("<li/>", {
      "class": "nav-item",
      "id": "cardNavTab" + i + "-" + obj.id,
    }).appendTo("#cardNav" + obj.id);
  }
  //creates <a>
  var active = "", selected = "";
  for (var i = 0; i < navTitles.length; i++) {
    if (i != 0) {
      active = "";
      selected = "false";
    } else {
      active = " active";
      selected = "true";
    }
    $("<a/>", {
      "class": "nav-link" + active,
      "href": "#cardTabPanel-" + navTitles[i] + "-" + obj.id,
      "aria-controls": "cardTabPanel-" + navTitles[i] + "-" + obj.id,
      "aria-selected": selected,
      "id": "cardPane-" + navTitles[i] + "-" + obj.id,
      "role": "tab",
      "data-toggle": "tab",
      "html": navTitles[i]
    }).appendTo("#cardNavTab" + i + "-" + obj.id);
  }
}

CardBuilder.createDivBody = function (obj) {
  $("<div/>", {
    "class": "card-body",
    "id": "cardBody" + obj.id,
  }).appendTo("#" + obj.appendId);
  return "cardBody" + obj.id;
}

CardBuilder.createDivTabContent = function (obj) {
  $("<div/>", {
    "class": "tab-content",
    "id": "cardTabContent" + obj.id
  }).appendTo("#" + obj.appendId);
  return "cardTabContent" + obj.id;
}

CardBuilder.createCardTabPanels = function (obj) {
  const navTitles = ["Overview", "Details", "About"];
  var active = "";
  for (var i = 0; i < navTitles.length; i++) {
    if (i != 0) {
      active = "";
    } else {
      active = " show active";
    }
    $("<div/>", {
      "class": "tab-pane fade" + active,
      "id": "cardTabPanel-" + navTitles[i] + "-" + obj.id,
      "role": "tabpanel",
      "aria-labelledby": "cardPane-" + navTitles[i] + "-" + obj.id
    }).appendTo("#" + obj.appendId);
  }
}

CardBuilder.createTitle = function (obj) {
  if (obj.h == "" || obj.h == null) {
    obj.h = 6;
  }
  $("<h" + obj.h + "/>", {
    "class": "card-title",
    "id": "cardTitle" + obj.id,
    "html": obj.text,
  }).appendTo("#" + obj.appendId);
  return "cardTitle" + obj.id;
}

CardBuilder.createText = function (obj) {
  if (obj.tag == "" || obj.tag == null) {
    obj.tag = "<p/>";
  }
  $(obj.tag, {
    "class": "card-text",
    "id": "cardText" + obj.id,
    "html": obj.text,
  }).appendTo("#" + obj.appendId);
  return "cardText" + obj.id;
}

CardBuilder.createDivFooter = function (obj) {
  $("<div/>", {
    "class": "card-footer",
    "id": "cardFooter" + obj.id,
    "html": obj.text,
  }).appendTo("#" + obj.appendId);
  return "cardFooter" + obj.id;
}

getEventData();
