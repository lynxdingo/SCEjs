sce.view.createEvent = {
	setupUserInterface: function () {
		var saveButton = document.forms['EventCorp'].commit;
		//load all event objects
		EventCorp.loadAll();
		//Set an event handler for the save submit button
		saveButton.addEventListener("click", sce.view.createEvent.handleSaveButtonClickEvent);
		window.addEventListener("beforeunload", function () {
			EventCorp.saveAll();
		});
	},
	handleSaveButtonClickEvent: function () {
		var formEl = document.forms['EventCorp'];
		var slots = {
			title: formEl.title.value,
			location: formEl.location.value,
			timeStart: convertTimepickiToValue("timeStart"),
			timeEnd: convertTimepickiToValue("timeEnd"),
			date: formEl.date.value,
			description: formEl.description.value,
			organizer: formEl.organizer.value,
			fee: formEl.fee.value,
			gender: formEl.gender.value,
			official: formEl.official.value,
			eventType: formEl.eventType.value,
		};

		//checks if all required inputs are filled
		if(!slots.title || !slots.location || !slots.timeStart || !slots.date){
			console.error("Not all required fields have inputs");
			return;
		}

		EventCorp.add(slots);
		formEl.reset();
	},
};

convertTimepickiToValue = function(id){
	const myId = document.getElementById(id);
	var myReturn = myId.getAttribute("data-timepicki-tim")+myId.getAttribute("data-timepicki-mini");
	if (myReturn.length == 4) {
		return myReturn;
	}
	return '';
}
