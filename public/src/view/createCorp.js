sce.view.createCorp = {
      setupUserInterface: function () {
            var saveButton = document.forms['Corporation'].commit;
            //load all event objects
            Corporation.loadAll();
            //Set an event handler for the save submit button
            saveButton.addEventListener("click", sce.view.createCorp.handleSaveButtonClickCorp);
            window.addEventListener("beforeunload", function () {
                  Corporation.saveAll();
            });
      },
      handleSaveButtonClickCorp: function () {
            var formEl = document.forms['Corporation'];
            var slots = {
                  name: formEl.name.value,
                  emblem: formEl.emblem.value,
                  coatOfArms: formEl.coatOfArms.value,
                  ribbonFellow: formEl.ribbonFellow.value,
                  ribbonFox: formEl.ribbonFox.value,
                  association: formEl.association.value,
                  motto: formEl.motto.value,
                  mottoHeraldic: formEl.mottoHeraldic.value,
                  emphasis: formEl.emphasis.value,
                  website: formEl.website.value,
                  city: formEl.city.value,
                  state: formEl.state.value,
                  country: formEl.country.value,
                  foundingDate: formEl.foundingDate.value,
                  gender: formEl.gender.value,
                  religion: formEl.religion.value,
                  address: formEl.address.value,
            };
            Corporation.add(slots);
            formEl.reset();
      },
      handlePopulateStateList: function (country) {
            var state = {
                  germany: [
                        {
                              "value": "BW",
                              "text": "Baden-Württemberg"
                        },
                        {
                              "value": "BY",
                              "text": "Bayern"
                        },
                        {
                              "value": "BE",
                              "text": "Berlin"
                        },
                        {
                              "value": "BB",
                              "text": "Brandenburg"
                        },
                        {
                              "value": "HB",
                              "text": "Bremen"
                        },
                        {
                              "value": "HH",
                              "text": "Hamburg"
                        },
                        {
                              "value": "HE",
                              "text": "Hessen"
                        },
                        {
                              "value": "MV",
                              "text": "Mecklenburg-Vorpommern"
                        },
                        {
                              "value": "NI",
                              "text": "Niedersachsen"
                        },
                        {
                              "value": "NW",
                              "text": "Nordrhein-Westfalen"
                        },
                        {
                              "value": "RP",
                              "text": "Rheinland-Pfalz"
                        },
                        {
                              "value": "SL",
                              "text": "Saarland"
                        },
                        {
                              "value": "SN",
                              "text": "Sachsen"
                        },
                        {
                              "value": "ST",
                              "text": "Sachsen-Anhalt"
                        },
                        {
                              "value": "SH",
                              "text": "Schwleswig-Holstein"
                        },
                        {
                              "value": "TH",
                              "text": "Thüringen"
                        },
                  ],
                  austria: [
                        {
                              "value": "Bgld",
                              "text": "Burgenland"
                        },
                        {
                              "value": "Ktn",
                              "text": "Kärnten"
                        },
                        {
                              "value": "NÖ",
                              "text": "Niederösterreich"
                        },
                        {
                              "value": "OÖ",
                              "text": "Oberösterreich"
                        },
                        {
                              "value": "Sbg",
                              "text": "Salzburg"
                        },
                        {
                              "value": "Stmk",
                              "text": "Steiermark"
                        },
                        {
                              "value": "T",
                              "text": "Tirol"
                        },
                        {
                              "value": "Vbg",
                              "text": "Vorarlberg"
                        },
                        {
                              "value": "W",
                              "text": "Wien"
                        }
                  ],
                  switzerland: [
                        {
                              "value": "ZH",
                              "text": "Zürich"
                        },
                        {
                              "value": "BE",
                              "text": "Bern"
                        },
                        {
                              "value": "LU",
                              "text": "Luzern"
                        },
                        {
                              "value": "UR",
                              "text": "Uri"
                        },
                        {
                              "value": "SZ",
                              "text": "Schwyz"
                        },
                        {
                              "value": "OW",
                              "text": "Obwalden"
                        },
                        {
                              "value": "NW",
                              "text": "Nidwalden"
                        },
                        {
                              "value": "GL",
                              "text": "Glarus"
                        },
                        {
                              "value": "ZG",
                              "text": "Zug"
                        },
                        {
                              "value": "FR",
                              "text": "Freiburg"
                        },
                        {
                              "value": "SO",
                              "text": "Solothurn"
                        },
                        {
                              "value": "BS",
                              "text": "Basel-Stadt"
                        },
                        {
                              "value": "BL",
                              "text": "Basel-Landschaft"
                        },
                        {
                              "value": "SH",
                              "text": "Schaffhausen"
                        },
                        {
                              "value": "AR",
                              "text": "Appenzell Ausserrhoden"
                        },
                        {
                              "value": "AI",
                              "text": "Appenzell Innerrhoden"
                        },
                        {
                              "value": "SG",
                              "text": "St. Gallen"
                        },
                        {
                              "value": "GR",
                              "text": "Graubünden"
                        },
                        {
                              "value": "AG",
                              "text": "Aargau"
                        },
                        {
                              "value": "TG",
                              "text": "Thurgau"
                        },
                        {
                              "value": "TI",
                              "text": "Tessin"
                        },
                        {
                              "value": "VD",
                              "text": "Waadt"
                        },
                        {
                              "value": "VS",
                              "text": "Wallis"
                        },
                        {
                              "value": "NE",
                              "text": "Neuenburg"
                        },
                        {
                              "value": "GE",
                              "text": "Genf"
                        },
                        {
                              "value": "JU",
                              "text": "Jura"
                        }
                  ]
            }
            $("<option/>", {
                  "value": "",
                  "html": " -- Staat -- "
            }).appendTo("#state");
            const cState = document.getElementById("state");
            switch (country) {
                  case "0049"://Deutschland
                        //$("#state").toggle();
                        if (cState.length != state.germany.length + 1) {
                              for (let i = cState.length; i > 0;i--) {
                                    cState.remove(i);
                              }
                        }
                        for (var i = 0; i < state.germany.length; i++) {
                              $("<option/>", {
                                    "value": state.germany[i].value,
                                    "html": state.germany[i].text
                              }).appendTo("#state");
                        }
                        console.log("List has " + cState.length);
                        break;

                  case "0043"://Österreich
                        //$("#state").toggle();
                        if (cState.length != state.austria.length + 1) {
                              for (let i = cState.length; i > 0;i--) {
                                    cState.remove(i);
                              }
                        }
                        for (var i = 0; i < state.austria.length; i++) {
                              $("<option/>", {
                                    "value": state.austria[i].value,
                                    "html": state.austria[i].text
                              }).appendTo("#state");
                        }
                        console.log("List has " + cState.length);
                        break;

                  case "0041"://Schweiz
                        //$("#state").toggle();
                        if (cState.length != state.switzerland.length + 1) {
                              for (let i = cState.length; i > 0;i--) {
                                    cState.remove(i);
                              }
                        }
                        for (var i = 0; i < state.switzerland.length; i++) {
                              $("<option/>", {
                                    "value": state.switzerland[i].value,
                                    "html": state.switzerland[i].text
                              }).appendTo("#state");
                        }
                        console.log("List has " + cState.length);
                        break;

                  default:
                        console.error("'" + country + "' DNE!");
                        break;
            }
      },
};
