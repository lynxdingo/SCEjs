sce.view.listEvents = {
	setupUserInterface: function(){
		var tableBodyEl = document.querySelector("table#events>tbody");
		var i=0, keys=[], key="", row={};
		//load all book objects
		EventCorp.loadAll();
		keys = Object.keys(EventCorp.instances);
		//for each book, create a table row with a cell for each attribute
		for(i=0;i<keys.length;i++){
			key = keys[i];
			row = tableBodyEl.insertRow();
			row.insertCell(-1).textContent = EventCorp.instances[key].eventID;
      row.insertCell(-1).textContent = EventCorp.instances[key].title;
      row.insertCell(-1).textContent = EventCorp.instances[key].location;
			row.insertCell(-1).textContent = EventCorp.instances[key].date;
			row.insertCell(-1).textContent = EventCorp.instances[key].timeStart;
		}
	},
	setupCardInterface: function(){
		var cardEl = document.querySelector("div#eventCards");
		var i=0, keys=[], key="";
		//loads all event objects
		EventCorp.loadAll();
		keys = Object.keys(EventCorp.instances);

	},
};
