sce.view.listCorps = {
	setupUserInterface: function(){
		var tableBodyEl = document.querySelector("table#corps>tbody");
		var i=0, keys=[], key="", row={};
		//load all book objects
		Corporation.loadAll();
		keys = Object.keys(Corporation.instances);
		//for each book, create a table row with a cell for each attribute
		for(i=0;i<keys.length;i++){
			key = keys[i];
			row = tableBodyEl.insertRow();
			row.insertCell(-1).textContent = Corporation.instances[key].corpID;
      row.insertCell(-1).textContent = Corporation.instances[key].name;
      row.insertCell(-1).textContent = Corporation.instances[key].country;
			row.insertCell(-1).textContent = Corporation.instances[key].foundingDate;
			row.insertCell(-1).textContent = Corporation.instances[key].address;
		}
	},
};
