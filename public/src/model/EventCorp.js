function EventCorp(slots) {
    this.title = slots.title;
    this.location = slots.location;
    this.timeStart = slots.timeStart;
    this.timeEnd = slots.timeEnd;
    this.date = slots.date;
    this.description = slots.description;
    this.organizer = slots.organizer;
    this.fee = slots.fee;
    this.gender = slots.gender;
    this.official = slots.official;
    this.corporation = slots.corporation || "this corporation";
    this.eventType = slots.eventType;
    this.eventID = EventCorp.generateId(slots);
}

EventCorp.add = function (slots) {
    const idLength = 12;
    slots.eventID = EventCorp.generateId(slots);
    if (slots.eventID.length == idLength) {
        var event = new EventCorp(slots);
        EventCorp.instances[slots.eventID] = event;
        console.log("Event " + slots.eventID + " created!");
    } else {
        console.error("Error generating id",slots.eventID);
    }

};

EventCorp.convertRow2Obj = function (eventRow) {
    var event = new EventCorp(eventRow);
    return event;
};

EventCorp.destroy = function (eventID) {
    if (EventCorp.instances[eventID]) {
        console.log("Event " + eventID + " deleted.");
        delete EventCorp.instances[eventID];
    } else {
        console.log("There is no event with ID " + eventID + " in the database!");
    }
};

EventCorp.generateId = function (slots) {
    /*return ""+this.corporation.country.code 
        + this.corporation.address.zipcode 
        + this.corporation.foundingDate*/
    var tString = slots.date + slots.timeStart;
    //removes all not allowed charachters
    tString = tString.replace(/[:-\s]/g, "");
    return tString;
};

EventCorp.instances = {};

EventCorp.loadAll = function () {
    var i = 0, key = "", keys = [], eventTableString = "", eventTable = {};
    try {
        if (localStorage["eventTable"]) {
            eventTableString = localStorage["eventTable"];
        }
    } catch (e) {
        alert("Error when reading from Local Storage\n" + e);
    }
    if (eventTableString) {
        eventTable = JSON.parse(eventTableString);
        keys = Object.keys(eventTable);
        console.log(keys.length + " events loaded");
        for (i = 0; i < keys.length; i++) {
            key = keys[i];
            EventCorp.instances[key] = EventCorp.convertRow2Obj(eventTable[key]);
        }
    }
};

EventCorp.read = function (eventID) {
    return EventCorp.instances[eventID];
};

EventCorp.saveAll = function () {
    var eventTableString = "", error = false,
        nmrOfEventCorps = Object.keys(EventCorp.instances).length;
    try {
        eventTableString = JSON.stringify(EventCorp.instances);
        localStorage["eventTable"] = eventTableString;
    } catch (e) {
        alert("Error when writing to Local Storage\n" + e);
        error = true;
    }
    if (!error) console.log(nmrOfEventCorps + " events saved.");
};

EventCorp.update = function (slots) {
    var event = EventCorp.instances[slots.eventID];
    var date = parseInt(slots.date);
    if (event.title !== slots.title) {
        event.date = date;
    }
    console.log("Event " + slots.eventID + " modified!");
};
