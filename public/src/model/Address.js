function Address(slots){
    this.regular = slots.regular;
    this.house = slots.house;
    this.alternative = slots.alternative;
};

Address.Regular = function(slots){
    this.street = slots.street;
    this.houseNumber = slots.houseNumber;
    this.zipcode = slots.zipcode;
    this.city = slots.city;
    this.country = slots.country;
};

Address.House = function(slots){
    this.street = slots.street;
    this.houseNumber = slots.houseNumber;
    this.zipcode = slots.zipcode;
    this.city = slots.city;
    this.country = slots.country;
};

Address.Alternative = function(slots){
    this.street = slots.street;
    this.houseNumber = slots.houseNumber;
    this.zipcode = slots.zipcode;
    this.city = slots.city;
    this.country = slots.country;
};

