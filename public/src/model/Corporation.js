function Corporation(slots) {
    this.name = slots.name;
    this.emblem = slots.emblem;
    this.coatOfArms = slots.coatOfArms;
    this.ribbonFellow = slots.ribbonFellow;
    this.ribbonFox = slots.ribbonFox;
    this.association = slots.association;
    this.motto = slots.motto;
    this.mottoHeraldic = slots.mottoHeraldic;
    this.emphasis = slots.emphasis;
    this.website = slots.website;
    this.city = slots.city;
    this.state = slots.state;
    this.country = slots.country;
    this.foundingDate = slots.foundingDate;
    this.gender = slots.gender;
    this.religion = slots.religion;
    this.address = slots.address;
    this.corpID = Corporation.generateId(slots);
}

Corporation.add = function (slots) {
    slots.corpID = Corporation.generateId(slots);
    var corp = new Corporation(slots);
    Corporation.instances[slots.corpID] = corp;
    console.log("Corp " + slots.corpID + " created!");
};

Corporation.convertRow2Obj = function (corpRow) {
    var corp = new Corporation(corpRow);
    return corp;
};

Corporation.destroy = function (corpID) {
    if (Corporation.instances[corpID]) {
        console.log("Corp " + corpID + " deleted.");
        delete Corporation.instances[corpID];
    } else {
        console.log("There is no corp with ID " + corpID + " in the database!");
    }
};

Corporation.generateId = function (slots) {
    /*return ""+slots.country.code
        + slots.address.zipcode
        + slots.foundingDate;*/
    var tString = slots.country + slots.address + slots.foundingDate;
    //a quick fix for removing the '-' charachter out of the string
    tString = tString.replace(/[:-\s]/g, "");
    return tString;
};

Corporation.instances = {};

Corporation.loadAll = function () {
    var i = 0, key = "", keys = [], corpTableString = "", corpTable = {};
    try {
        if (localStorage["corpTable"]) {
            corpTableString = localStorage["corpTable"];
        }
    } catch (e) {
        alert("Error when reading from Local Storage\n" + e);
    }
    if (corpTableString) {
        corpTable = JSON.parse(corpTableString);
        keys = Object.keys(corpTable);
        console.log(keys.length + " corporations loaded");
        for (i = 0; i < keys.length; i++) {
            key = keys[i];
            Corporation.instances[key] = Corporation.convertRow2Obj(corpTable[key]);
        }
    }
};

Corporation.read = function (corpID) {
    return Corporation.instances[corpID];
};

Corporation.saveAll = function () {
    var corpTableString = "", error = false,
        nmrOfCorporations = Object.keys(Corporation.instances).length;
    try {
        corpTableString = JSON.stringify(Corporation.instances);
        localStorage["corpTable"] = corpTableString;
    } catch (e) {
        alert("Error when writing to Local Storage\n" + e);
        error = true;
    }
    if (!error) console.log(nmrOfCorporations + " corporations saved.");
};

Corporation.update = function (slots) {
    var corp = Corporation.instances[slots.corpID];
    var year = parseInt(slots.year);
    if (corp.title !== slots.title) {
        corp.year = year;
    }
    console.log("Corp " + slots.corpID + " modified!");
};
