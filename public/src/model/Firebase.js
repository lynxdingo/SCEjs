var firestore = firebase.firestore();

var autoID = 100;

const docRef = firestore.doc("testEvents/5/");
const inputTextfield = document.querySelector("#fbInput");
const outputHeader = document.querySelector("#fbOutput");
const submitBtn = document.querySelector("#fbSubmit");
const colEvent = "testEvents";
const colCorporation = "testCorporations";

var testEvent = {
    eventID: ""+autoID,
    title: "Test title",
    location: "Hause",
    timeStart: "16:15",
    timeEnd: null,
    date: "2018-15-05",
    description: "This is a test event",
    fee: null,
    gender: "Male",
    official: "offiziell",
    corporation: null,
    eventType: "Kneipe",
}

function FirestoreIO(obj,collection){
  this.obj = obj;
  this.collection = collection;
}

FirestoreIO.postCollection = function(obj,col){
  switch (col) {
    case "EVENT":
      writeEventData(obj);
      break;
    case "CORPORATION":
      writeCorporationData(obj);
      break;
    default:
      console.error("Collection: "+col+" DNE.");
  }
}

FirestoreIO.getCollection = function(obj,col){
  switch (col) {
    case "EVENT":
      readEventData(obj);
      break;
    case "CORPORATION":
      readCorporationData(obj);
      break;
    default:
      console.error("Collection: "+col+" DNE.");
  }
}

function writeEventData(event) {
    firestore.collection(colEvent).doc(event.eventID).set({
        id: event.eventID,
        title: event.title,
        location: event.location,
        timeStart: event.timeStart,
        timeEnd: event.timeEnd,
        date: event.date,
        description: event.description,
        fee: event.fee,
        gender: event.gender,
        official: event.official,
        corporation: event.corporation,
        eventType: event.eventType
    }).then(function () {
        console.log("Event " + event.eventID + " saved!");
        testEvent.eventID = ""+autoID++;
    }).catch(function (error) {
        console.error("Error occured: ", error);
    });
};

function writeCorporationData(corp){
  firestore.collection(colCorporation).doc(corp.corpID).set({
    id: corp.corpID,
    name: corp.name,
    emblem: corp.emblem,
    coatOfArms: corp.coatOfArms,
    ribbonFellow: corp.ribbonFellow,
    ribbonFox: corp.ribbonFox,
    association: corp.association,
    motto: corp.motto,
    mottoHeraldic: corp.mottoHeraldic,
    emphasis: corp.emphasis,
    website: corp.website,
    city: corp.city,
    state: corp.state,
    country: corp.country,
    foundingDate: corp.foundingDate,
    gender: corp.gender,
    religion: corp.religion,
    address: corp.address,
  }).then(function(){
    console.log("Corp "+corp.corpID+" saved!");
  }).catch(function(error){
    console.error("Error occured: ",error);
  });
}

function readEventData(event){
  firestore.collection(colEvent).doc(event.eventID).onSnapshot(function (doc) {
      if (doc && doc.exists) {
          const myData = doc.data();
          outputHeader.innerText = "Text Added: " + myData.title;
      }
  });
}

function readCorporationData(corp){
  firestore.collection(colCorporation).doc(corp.corpID).onSnapshot(function (doc) {
      if (doc && doc.exists) {
          const myData = doc.data();
          outputHeader.innerText = "Text Added: " + myData.title;
      }
  });
}

function readEventOnce() {
    var userId = firebase.auth().currentUer.uid;
    return firebase.database().ref('/events/' + userId).once('value').then(function (snapshot) {
        var username = (snapshot.val() && snapshot.val().username) || 'Anonymous';
    });
}

// ----- From index.js ----- //

/*submitBtn.addEventListener("click", function (error) {
    const textTosubmit = inputTextfield.value;
    console.log("Text to submit: " + textTosubmit);
    docRef.set({
        hotDogStatus: textTosubmit
    }).then(function () {
        console.log("Status saved!");
    }).catch(function () {
        console.error("Got an error: ", error);
    })
});*/

submitBtn.addEventListener("click", function (error) {
    writeEventData(testEvent);
});

getRealtimeUpdates = function () {
    firestore.doc(colEvent+"/"+autoID).onSnapshot(function (doc) {
        if (doc && doc.exists) {
            const myData = doc.data();
            outputHeader.innerText = "Text Added: " + myData.title;
        }
    });
}

getRealtimeUpdates();
